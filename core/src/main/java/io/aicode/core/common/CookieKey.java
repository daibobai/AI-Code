/*
 *
 *                       http://www.aicode.io
 *
 *
 *      本代码仅用于AI-Code.
 */

package io.aicode.core.common;

/**
 * cookie 键
 * Created by lixin on 2017/6/3.
 */
public class CookieKey {
    public static final String UploadUidKey = "Upload-Uid";
}
