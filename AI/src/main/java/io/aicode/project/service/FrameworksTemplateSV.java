/*
 * Powered By [lixin]
 *
 */

package io.aicode.project.service;

import io.aicode.core.base.BaseMybatisSV;
import io.aicode.project.entity.FrameworksTemplate;

public interface FrameworksTemplateSV extends BaseMybatisSV<FrameworksTemplate, Long> {

}
