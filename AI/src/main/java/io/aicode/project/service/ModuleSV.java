/*
 * Powered By [lixin]
 *
 */

package io.aicode.project.service;

import io.aicode.core.base.BaseMybatisSV;
import io.aicode.project.entity.Module;

public interface ModuleSV extends BaseMybatisSV<Module, Long> {

}
