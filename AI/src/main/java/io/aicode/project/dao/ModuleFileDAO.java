/*
 * Powered By [lixin]
 *
 */

package io.aicode.project.dao;


import io.aicode.core.base.BaseMybatisDAOImpl;
import io.aicode.project.entity.ModuleFile;
import org.springframework.stereotype.Repository;


@Repository
public class ModuleFileDAO extends BaseMybatisDAOImpl<ModuleFile,Long> {


}
